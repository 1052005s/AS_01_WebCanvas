# Software Studio 2018 Spring Assignment 01 Web Canvas

## Web Canvas
<img src="preview.png" width="882px" height="781px" alt="preview"></img>

## Components
<h3> 基本功能 </h3>
1. <b>繪圖筆刷</b>
    <img src="pic/brushsize.png" width="68px" height="60px" alt="preview"></img>
    * 可變更筆刷顏色
    * 可調整筆刷大小
    
2. <b>橡皮擦</b>
    <img src="pic/eraser.png" width="68px" height="60px" alt="preview"></img>
    * 可清除選擇路徑上的畫布
 
3. <b>文字輸入</b>
    <img src="pic/txt.png" width="68px" height="60px" alt="preview"></img>
    * 使用者可自定義文字加入畫布中
    * 自定義選項： 正常/斜體，字型(Arial, sans-serif, serif, Verdana), 字體大小, 字體顏色, 插入位置(畫布座標輸入)
    
4. <b>鼠標樣式</b>
    *  鼠標樣式隨使用者選擇的畫布工具改變(crosshair, alias, cell, move, zoom-in)，方便使用者辨認

5. <b>畫布重置</b>  <img src="pic/clear.png" width="68px" height="60px" alt="preview"></img>
    * 清除畫布上所有的內容

 <h3>進階功能</h3>
6. <b>繪圖筆刷樣式</b>
    <img src="pic/circle.png" width="68px" height="60px" alt="preview"></img>
    <img src="pic/rec.png" width="68px" height="60px" alt="preview"></img>
    <img src="pic/triangle.png" width="68px" height="60px" alt="preview"></img>
    * 提供圓形, 正方形, 三角形讓使用者方便作畫

7. <b>Un/Re-do功能</b>
    <img src="pic/undo.png" width="68px" height="60px" alt="preview"></img>
    <img src="pic/redo.png" width="68px" height="60px" alt="preview"></img>
    * 恢復前/後一個畫布內容

8. <b>畫布下載</b>
    <img src="pic/save.png" width="68px" height="60px" alt="preview"></img>
    * 下載當前的畫布內容 (.jpg)

## Student ID , Name and Template URL
-  Student ID : 1052005s
-  Name : 林浩平
-  URL : https://1052005s.gitlab.io/AS_01_WebCanvas
